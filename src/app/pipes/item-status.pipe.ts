import {Pipe, PipeTransform} from "@angular/core";
import { ItemState } from '../enums/item-state';

@Pipe({
    name: "ItemStatus"
})
export class ItemStatusPipe implements PipeTransform {

    transform(value: ItemState): string {
        switch (value) {
          case ItemState.CLEAN:
            return "green";
          case ItemState.DIRTY:
            return "brown";
          case ItemState.REPLACE:
            return "red";
        }
    }

}
