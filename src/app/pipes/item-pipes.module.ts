import {NgModule} from "@angular/core";
import {ItemStatusPipe} from "./item-status.pipe";

@NgModule({
    declarations: [
        ItemStatusPipe
    ],
    exports: [
        ItemStatusPipe
    ]
})
export class ItemPipesModule {
}
