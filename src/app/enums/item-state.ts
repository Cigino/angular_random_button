export enum ItemState {
  CLEAN = "clean",
  DIRTY = "dirty",
  REPLACE = "replace"
}
