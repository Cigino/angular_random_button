import { ItemState } from '../enums/item-state';

export interface Item {
  name: string;
  status: ItemState;
}
