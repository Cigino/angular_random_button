import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observer, take } from 'rxjs';
import { Item } from './interfaces/item';
import { RandomService } from './services/random.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  public items?: Item[];
  public form!: FormGroup;
  public n: FormControl = new FormControl("", [Validators.required, Validators.max(10), Validators.min(1)]);
  public error: boolean = false;

  constructor(private randomService: RandomService,
              private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      n: this.n,
    });
  }

  public loadData() {
    const observer: Observer<Item[]> = {
      next: (data) => this.items = this.sortData(data),
      error: () => {
        this.form.controls["n"].setValue(+this.form.value["n"] - 1);
        this.error = true;
      },
      complete: () => {}
    }

    if (this.form.valid) {
      this.items = undefined;
      this.error = false;
      this.randomService.getData().pipe(take(1)).subscribe(observer);
    }
  }

  private sortData(items: Item[]): Item[] {
    if (!items) {
      return items;
    }
    return items.sort((i1, i2) => {
      const comparison = i1.status.localeCompare(i2.status);
      if (comparison === 0) {
        return i1.name.localeCompare(i2.name);
      } else {
        return comparison;
      }
    });
  }
}
