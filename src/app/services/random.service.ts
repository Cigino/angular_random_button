import {Injectable} from "@angular/core";
import { Observable, Subject } from 'rxjs';
import { Item } from '../interfaces/item';
import { itemList } from './data/items';

@Injectable({
    providedIn: "root"
})
export class RandomService {
  private data$$: Subject<Item[]> = new Subject();

    public getData(): Observable<Item[]> {
      setTimeout(() => {
        if (Math.random() < 0.5) {
          this.data$$.next(itemList);
        } else {
          this.data$$.error(new Error("Error"));
          this.data$$.unsubscribe();
          this.data$$ = new Subject();
        }
      }, 2000);
      return this.data$$.asObservable();
    }
}
